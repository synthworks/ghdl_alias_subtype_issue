# An analysis of 'subtype and alias in GHDL
This analysis includes executable code.  
The scripts require running OSVVM's build script environment.

[[_TOC_]]


## Running the OSVVM build script environment
Clone the repository:
```
git clone https://gitlab.com/synthworks/ghdl_alias_subtype_issue.git
```

You will also need to clone the OsvvmLibraries repository and checkout the dev branch Scripts:
```
git clone --recursive https://github.com/osvvm/OsvvmLibraries
cd OsvvmLibraries/Scripts
git checkout dev
```

Go to ghdl_alias_subtype_issue/sim:
```
cd ghdl_alias_subtype_issue/sim
```

Now run GHDL at your linux or msys2 prompt:
```
winpty rlwrap tclsh
source ../../OsvvmLibraries/Scripts/StartUp.tcl
```

Now run tests by saying build and their directory name.
Tests 2b and 2g fail.  The rest pass.
```
build ../test_1a_unsigned_port
build ../test_1b_unsigned_alias
build ../test_2a_record_subtype
build ../test_2b_record_subtype_alias  # Fails
build ../test_2c_record_range_alias
build ../test_2d_record_subtype_length_decl
build ../test_2e_record_simple_alias
build ../test_2f_record_constrained_alias
build ../test_2g_record_alias_port  # Fails
```

Test output will be in the directory:
`logs/GHDL-<revision>`

## Test Case 1:  Unconstrained port of type unsigned
### Subcase 1a:  use 'subtype
Conclusion:  'subtype seems to work fine.

```vhdl
entity test is
  port(
    input : in unsigned
  );
end entity;

architecture rtl of test is
  signal copy : input'subtype;
begin
  copy <= input ;
  
  process 
  begin 
    wait on copy ;  -- Suppress first run
    report "Copy = " & to_hstring(Copy) ; 
  end process ; 
  
end architecture;
```

### Subcase 1b:  use 'subtype and alias
Conclusion:  'subtype and alias seems to work fine here.

```vhdl
entity test is
  port(
    input : in unsigned);
end entity;

architecture rtl of test is
  signal copy : input'subtype;
  
  alias B is copy ; 
begin
  copy <= input ;
  
  process 
  begin 
    wait on copy ;  -- Suppress first run
    report "Copy, B = " & to_hstring(Copy) & ", " & to_hstring(B) ; 
  end process ; 
  
end architecture;
```

## Test Case 2:  Record with Unconstrained array elements
For this test case I created the following package.
```vhdl
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package TestPkg is
  type ARecType is record
    A : unsigned ;
  end record ARecType ; 

end package TestPkg ; 
```

### Subcase 2a:  use 'subtype 
Using 'subtype and referencing an object of that subtype works fine.
```vhdl
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.TestPkg.all ;

entity test is
  port(
    input : in ARecType 
  );
end entity;

architecture rtl of test is
  signal copy : input'subtype;
begin
  copy <= input ;
  
  process 
  begin 
    wait on copy ;  -- Suppress first run
    report "Copy.A = " & to_hstring(Copy.A) ; 
  end process ; 
  
end architecture;
```
### Subcase 2b:  use 'subtype plus alias  -- Fails
Conclusion:  This test case fails to compile

```vhdl
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.TestPkg.all ;

entity test is
  port(
    input : in ARecType 
  );
end entity;

architecture rtl of test is
  signal copy : input'subtype;   
  alias B is copy ; 

begin
  copy <= input ;
  
  process 
  begin 
    wait on copy ;  -- Suppress first run
    report "Copy.A, B.A = " & to_hstring(Copy.A) & ", " & to_hstring(B.A) ; 
  end process ; 
  
end architecture;
```

It should be noted that in place of the `alias B ...` 
I tried each of the following.  These are important 
to the root cause of the issue.
```vhdl
alias B : ARecType(A(input.A'range)) is copy ;  
```
```vhdl
alias B : ARecType(A(7 downto 0)) is copy ;  
```
```vhdl
subtype BType is ARecType(A(7 downto 0)) ;
alias B : BType is copy ;
```

These all resulted in the error:  
```
.\tbtest:error: bound check failure at ../../src/ieee2008/std_logic_1164-body.vhdl:905
```



### Subcase 2c:  use 'range plus alias
Using 'range rather than 'subtype fixes the issue 
with alias. 

```vhdl
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.TestPkg.all ;

entity test is
  port(
    input : in ARecType 
  );
end entity;

architecture rtl of test is
  signal copy : ARecType(A(input.A'range)) ;
  
  alias B is copy ; 

begin
  copy <= input ;
  
  process 
  begin 
    wait on copy ;  -- Suppress first run
    report "Copy.A, B.A = " & to_hstring(Copy.A) & ", " & to_hstring(B.A) ; 
  end process ; 
  
end architecture;
```

### Subcase 2d:  use 'subtype plus 'length on the subtype in declaration
Conclusion:  This works.  As a result, an object that 
is declared using 'subtype clearly has the correct 
subtype infomration.

```vhdl
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.TestPkg.all ;

entity test is
  port(
    input : in ARecType 
  );
end entity;

architecture rtl of test is
  signal copy : input'subtype;  

  constant B : integer := copy.A'length ;   
begin
  copy <= input ;
  
  process 
  begin 
    wait on copy ;  -- Suppress first run
    report "Copy.A, B = " & to_hstring(Copy.A) & ", " & to_hstring(B) ; 
  end process ; 
  
end architecture;
```

### Subcase 2e:  record scalar alias
Conclusion:  Changed the record element to integer.
Used code similar to 2b.  Works just fine.
See also the code in 2f.


### Subcase 2f:  record constrained alias
Conclusion:  Added a constraint to the unsigned
record element.   Now the alias works just fine.


```vhdl
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.TestPkg.all ;

entity test is
  port(
    input : in ARecType 
  );
end entity;

architecture rtl of test is
  signal copy : input'subtype;   
  alias B is copy ; 

begin
  copy <= input ;
  
  process 
  begin 
    wait on copy ;  -- Suppress first run
    report "Copy.A, B.A = " & to_hstring(Copy.A) & ", " & to_hstring(B.A) ; 
  end process ; 
  
end architecture;
```

### Subcase 2g:  record alias to port  -- Fails
Conclusion:  This is 2b with the alias to the port.
It too fails.

```vhdl
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.TestPkg.all ;

entity test is
  port(
    input : in ARecType 
  );
end entity;

architecture rtl of test is
  alias B is input ; 

  signal copy : input'subtype;   

begin
  copy <= input ;
  
  process 
  begin 
    wait on copy ;  -- Suppress first run
    report "Copy.A, B.A = " & to_hstring(Copy.A) & ", " & to_hstring(B.A) ; 
  end process ; 
  
end architecture;
```

It should be noted that in place of the `alias B ...` 
I tried each of the following.  These are important 
to the root cause of the issue.
```vhdl
alias B : ARecType(A(input.A'range)) is input ;  
```
```vhdl
alias B : ARecType(A(7 downto 0)) is input ;  
```
```vhdl
subtype BType is ARecType(A(7 downto 0)) ;
alias B : BType is input ;
```

These all resulted in the error:  
```
.\tbtest:error: bound check failure at ../../src/ieee2008/std_logic_1164-body.vhdl:905
```


